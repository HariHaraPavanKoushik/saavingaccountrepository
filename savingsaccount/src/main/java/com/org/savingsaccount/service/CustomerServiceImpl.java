package com.org.savingsaccount.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.savingsaccount.dto.BankStatementResponseDto;
import com.org.savingsaccount.dto.CustomerDto;
import com.org.savingsaccount.dto.FundTransferRequestDto;
import com.org.savingsaccount.dto.LoginRequestDto;
import com.org.savingsaccount.dto.SelfAccountTransactionResponseDto;
import com.org.savingsaccount.entity.Beneficiary;
import com.org.savingsaccount.entity.Customer;
import com.org.savingsaccount.entity.FundTransfer;
import com.org.savingsaccount.entity.SelfAccountTransaction;
import com.org.savingsaccount.exception.BeneficiaryAccountNotFoundException;
import com.org.savingsaccount.exception.BeneficiaryAlreadyExists;
import com.org.savingsaccount.exception.FundsException;
import com.org.savingsaccount.exception.ListEmptyException;
import com.org.savingsaccount.exception.UserAccountNotFoundException;
import com.org.savingsaccount.exception.UserExistsErrorException;
import com.org.savingsaccount.exception.UserNotLoginException;
import com.org.savingsaccount.repository.BeneficiaryRepository;
import com.org.savingsaccount.repository.CustomerRepository;
import com.org.savingsaccount.repository.FundTransferRepository;
import com.org.savingsaccount.repository.SelfAccountTransactionRepository;
import com.org.savingsaccount.utility.CustomerUtility;
import com.org.savingsaccount.utility.SavingUtility;

@Service
public class CustomerServiceImpl implements CustomerService {
	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	BeneficiaryRepository beneficiaryRepository;

	@Autowired
	FundTransferRepository fundTransferRepository;

	@Autowired
	SelfAccountTransactionRepository selfAccountTransactionRepository;

	private static final Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);
	String accountNumber;

	/**
	 * 
	 *
	 * @param emailId
	 * @param password
	 * @return CustomerDto Object along with status code
	 * @throws CustomException
	 * @throws LoginException
	 * @throws CustomException
	 * 
	 * @author Pallavi this method is used for login
	 * @throws UserExistsErrorException 
	 *
	 */
	@Override
	public CustomerDto customerLogin(LoginRequestDto loginRequestDto) throws UserExistsErrorException {
		logger.info("Inside CustomerServiceImpl of customerLogin method ");
		CustomerDto customerDto = new CustomerDto();
		Optional<Customer> userDetail = customerRepository.findByEmailIdAndPassword(loginRequestDto.getEmail(),
				loginRequestDto.getPassword());
		accountNumber = userDetail.get().getAccountNumber();
		if (!userDetail.isPresent()) {
			throw new UserExistsErrorException(CustomerUtility.USER_ERROR);
		}
		customerDto.setCustomerName(userDetail.get().getCustomerName());
		customerDto.setAccountNumber(userDetail.get().getAccountNumber());
		customerDto.setBankName(userDetail.get().getBankName());
		customerDto.setIfscCode(userDetail.get().getIfscCode());
		customerDto.setBalance(userDetail.get().getBalance());
		return customerDto;
	}

	/**
	 * 
	 *
	 * @requestBody fundTransferRequestDto
	 * 
	 * @return status code and message
	 * @throws CustomException
	 * @throws LoginException
	 * @author Kusuma this method is used for fundtransfer
	 * @throws BeneficiaryAccountNotFoundException 
	 * @throws UserAccountNotFoundException 
	 * @throws FundsException 
	 * 
	 *
	 */

	@Override
	@Transactional
	public String fundTransfer(FundTransferRequestDto fundTransferRequestDto) throws BeneficiaryAccountNotFoundException, UserAccountNotFoundException, FundsException  {
		logger.info("Inside CustomerServiceImpl of fundTransfer method ");
		FundTransfer fundTransfer = new FundTransfer();

		Optional<Beneficiary> optionalBeneficiary = beneficiaryRepository
				.findByBeneficiaryAccountNumberAndFromAccountNumber(fundTransferRequestDto.getToAccountNumber(),
						fundTransferRequestDto.getFromAccountNumber());
		Optional<Customer> optionalCustomerFromAccount = customerRepository
				.findByAccountNumber(fundTransferRequestDto.getFromAccountNumber());
		Optional<Customer> optionalCustomerToAccount = customerRepository
				.findByAccountNumber(fundTransferRequestDto.getToAccountNumber());

		/** checking if beneficiary is present, Account NUmber Not Present */
		if (!optionalBeneficiary.isPresent()) {
			throw new BeneficiaryAccountNotFoundException(SavingUtility.BENEFICIARY_ACCOUNT_NUMBER_NOT_FOUND);
		}

		if (!optionalCustomerFromAccount.isPresent()) {
			throw new UserAccountNotFoundException(SavingUtility.USER_ACCOUNT_NUMBER_NOT_FOUND);
		}
		if (!optionalCustomerToAccount.isPresent()) {
			throw new BeneficiaryAccountNotFoundException(SavingUtility.BENEFICIARY_ACCOUNT_NUMBER_NOT_FOUND);
		}

		if (optionalCustomerFromAccount.get().getBalance() < fundTransferRequestDto.getAmount()
				|| optionalCustomerFromAccount.get().getBalance() <= 1000) {
			throw new FundsException(CustomerUtility.INSUFFICIENT_BALANCE);
		}
		double fromUpdatedBalance = optionalCustomerFromAccount.get().getBalance() - fundTransferRequestDto.getAmount();
		double toUpdatedBalance = optionalCustomerToAccount.get().getBalance() + fundTransferRequestDto.getAmount();

		optionalCustomerFromAccount.get().setBalance(fromUpdatedBalance);
		customerRepository.save(optionalCustomerFromAccount.get());
		optionalCustomerToAccount.get().setBalance(toUpdatedBalance);
		customerRepository.save(optionalCustomerToAccount.get());

		fundTransfer.setAmount(fundTransferRequestDto.getAmount());
		fundTransfer.setDate(LocalDateTime.now());
		fundTransfer.setFromAccountNumber(fundTransferRequestDto.getFromAccountNumber());
		fundTransfer.setToAccountNumber(fundTransferRequestDto.getToAccountNumber());
		fundTransferRepository.save(fundTransfer);

		return "Transferred";
	}

	/**
	 * 
	 *
	 * @param accountNumber
	 * @param beneficiaryAccountNumber
	 * 
	 * @return status code and message
	 * @throws CustomException
	 * 
	 * @author suprita this method is used for adding beneficiaries
	 * @throws BeneficiaryAlreadyExists 
	 * @throws UserAccountNotFoundException 
	 * @throws BeneficiaryAccountNotFoundException 
	 *
	 */
	@Override
	public String addBeneficiary(String accountNumber, String beneficiaryAccountNumber) throws BeneficiaryAlreadyExists, UserAccountNotFoundException, BeneficiaryAccountNotFoundException  {
		logger.info("Inside CustomerServiceImpl of addBeneficiary method ");
		Optional<Customer> user = customerRepository.findByAccountNumber(accountNumber);

		if (!user.isPresent()) {
			throw new UserAccountNotFoundException(SavingUtility.USER_ACCOUNT_NUMBER_NOT_FOUND);

		}

		Optional<Customer> beneficiary2 = customerRepository.findByAccountNumber(beneficiaryAccountNumber);

		if (!beneficiary2.isPresent()) {
			throw new BeneficiaryAccountNotFoundException(SavingUtility.BENEFICIARY_ACCOUNT_NUMBER_NOT_FOUND);

		}

		Optional<Beneficiary> beneficiary1 = beneficiaryRepository
				.findByBeneficiaryAccountNumber(beneficiaryAccountNumber);

		if (beneficiary1.isPresent()) {
			throw new BeneficiaryAlreadyExists(SavingUtility.BENEFICIARY_ACCOUNT_NUMBER_EXISTS);

		}
		Beneficiary beneficiary = new Beneficiary();
		beneficiary.setFromAccountNumber(accountNumber);
		beneficiary.setBeneficiaryAccountNumber(beneficiaryAccountNumber);
		beneficiary.setBankName(beneficiary2.get().getBankName());
		beneficiary.setIfscCode(beneficiary2.get().getIfscCode());
		beneficiary.setBeneficiaryId(beneficiary2.get().getCustomerId());

		beneficiaryRepository.save(beneficiary);

		return SavingUtility.BENEFICIARY_ADDED_SUCCESS;
	}

	@Override
	public List<Object> bankStatement(String accountNumber) throws ListEmptyException, UserExistsErrorException{

		Optional<List<FundTransfer>> listOfFundTransfer = fundTransferRepository
				.findByFromAccountNumberOrToAccountNumber(accountNumber, accountNumber);
		if (!listOfFundTransfer.isPresent()) {
			throw new ListEmptyException(SavingUtility.LIST_EMPTY);

		}
		List<Object> listOfBankStatementResponseDto = new ArrayList<>();
		for (FundTransfer fundTransfer : listOfFundTransfer.get()) {
			BankStatementResponseDto bankStatementResponseDto = new BankStatementResponseDto();
			if (fundTransfer.getFromAccountNumber().equalsIgnoreCase(accountNumber)) {
				bankStatementResponseDto.setType("DEBIT");
			} else if (fundTransfer.getToAccountNumber().equalsIgnoreCase(accountNumber)) {
				bankStatementResponseDto.setType("CREDIT");
			}

			BeanUtils.copyProperties(fundTransfer, bankStatementResponseDto);
			listOfBankStatementResponseDto.add(bankStatementResponseDto);
		}

		Optional<List<SelfAccountTransaction>> optionalListSelfAccount = selfAccountTransactionRepository
				.findByAccountNumber(accountNumber);
		if (!optionalListSelfAccount.isPresent()) {
			throw new ListEmptyException(SavingUtility.LIST_EMPTY);
		}

		List<SelfAccountTransactionResponseDto> listSelfAccountTransactionResponseDto = new ArrayList<>();
		for (SelfAccountTransaction selfAccountTransaction : optionalListSelfAccount.get()) {
			SelfAccountTransactionResponseDto selfAccountTransactionResponseDto = new SelfAccountTransactionResponseDto();

			if (selfAccountTransaction.getTransactionType().equalsIgnoreCase("WITHDRAW")) {
				selfAccountTransactionResponseDto.setTransactionType("WITHDRAW");
			} else if (selfAccountTransaction.getTransactionType().equalsIgnoreCase("DEPOSIT")) {
				selfAccountTransactionResponseDto.setTransactionType("DEPOSIT");
			}
			

			BeanUtils.copyProperties(selfAccountTransaction, selfAccountTransactionResponseDto);

			listSelfAccountTransactionResponseDto.add(selfAccountTransactionResponseDto);
			
		}
		listOfBankStatementResponseDto.addAll(listSelfAccountTransactionResponseDto);
		return listOfBankStatementResponseDto;
	}

	@Override
	public List<String> beneficiaryList() throws UserNotLoginException {
		if (accountNumber == null)
			throw new UserNotLoginException(CustomerUtility.USERNOTLOGIN_ERROR);

		List<Beneficiary> beneficiaryList = beneficiaryRepository.findByFromAccountNumber(accountNumber);

		List<String> beneficiaryAccountNumberList = new ArrayList<String>();

		for (Beneficiary beneficiary : beneficiaryList)
			beneficiaryAccountNumberList.add(beneficiary.getBeneficiaryAccountNumber());

		return beneficiaryAccountNumberList;
	}
	
	
}
