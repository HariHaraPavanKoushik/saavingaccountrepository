package com.org.savingsaccount.service;

import java.util.List;

import com.org.savingsaccount.dto.CustomerDto;
import com.org.savingsaccount.dto.FundTransferRequestDto;
import com.org.savingsaccount.dto.LoginRequestDto;
import com.org.savingsaccount.exception.UserExistsErrorException;
import com.org.savingsaccount.exception.UserNotLoginException;
import com.org.savingsaccount.exception.BeneficiaryAccountNotFoundException;
import com.org.savingsaccount.exception.BeneficiaryAlreadyExists;
import com.org.savingsaccount.exception.FundsException;
import com.org.savingsaccount.exception.ListEmptyException;
import com.org.savingsaccount.exception.UserAccountNotFoundException;

public interface CustomerService {
	

	public String fundTransfer(FundTransferRequestDto fundTransferRequestDto) throws BeneficiaryAccountNotFoundException, UserAccountNotFoundException, FundsException;

	String addBeneficiary(String accountNumber, String beneficiaryAccountNumber) throws UserExistsErrorException, UserAccountNotFoundException, BeneficiaryAccountNotFoundException, BeneficiaryAlreadyExists;

	List<Object> bankStatement(String accountNumber) throws ListEmptyException, UserExistsErrorException;

	CustomerDto customerLogin(LoginRequestDto loginRequestDto) throws UserExistsErrorException;

	List<String> beneficiaryList() throws UserNotLoginException;
}
