package com.org.savingsaccount.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
public class Beneficiary {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long beneficiaryId;
	private String fromAccountNumber;
	private String beneficiaryAccountNumber;
	private String bankName;
	private String ifscCode;

}
