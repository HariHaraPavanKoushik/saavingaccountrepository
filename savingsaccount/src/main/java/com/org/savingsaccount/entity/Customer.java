package com.org.savingsaccount.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
public class Customer {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long customerId;
	private String accountNumber;
	private String customerName;
	private String emailId;
	private String password;
	private String phoneNumber;
	private String bankName;
	private String ifscCode;
	private double balance;
	private String accountType;

}
