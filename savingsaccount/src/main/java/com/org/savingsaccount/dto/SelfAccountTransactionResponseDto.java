package com.org.savingsaccount.dto;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SelfAccountTransactionResponseDto {

	private String accountNumber;
	private double amount;
	private String transactionType;
	private LocalDateTime date;
}
