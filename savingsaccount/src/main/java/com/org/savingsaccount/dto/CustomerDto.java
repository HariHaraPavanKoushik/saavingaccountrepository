package com.org.savingsaccount.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CustomerDto {
	private String accountNumber;
	private String customerName;
	private String bankName;
	private String ifscCode;
	private double balance;
}
