package com.org.savingsaccount.dto;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class BankStatementResponseDto {

	private String fromAccountNumber;
	private String toAccountNumber;
	private double amount;
	private LocalDateTime date;
	private String type;

}
