package com.org.savingsaccount.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class FundTransferRequestDto {

	private String fromAccountNumber;
	private String toAccountNumber;
	private double amount;

}
