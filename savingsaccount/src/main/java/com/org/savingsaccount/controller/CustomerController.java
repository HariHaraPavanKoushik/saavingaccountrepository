package com.org.savingsaccount.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.org.savingsaccount.dto.CustomerDto;
import com.org.savingsaccount.dto.FundTransferRequestDto;
import com.org.savingsaccount.dto.LoginRequestDto;
import com.org.savingsaccount.exception.UserExistsErrorException;
import com.org.savingsaccount.exception.UserNameCredentialException;
import com.org.savingsaccount.exception.UserNotLoginException;
import com.org.savingsaccount.exception.BeneficiaryAccountNotFoundException;
import com.org.savingsaccount.exception.BeneficiaryAlreadyExists;
import com.org.savingsaccount.exception.CredentialErrorException;
import com.org.savingsaccount.exception.FundsException;
import com.org.savingsaccount.exception.ListEmptyException;
import com.org.savingsaccount.exception.UserAccountNotFoundException;
import com.org.savingsaccount.service.CustomerService;

@RestController
@RequestMapping("/customer")
public class CustomerController {

	private static final Logger logger = LoggerFactory.getLogger(CustomerController.class);

	@Autowired
	CustomerService customerService;

	/**
	 * 
	 *
	 * @param emailId
	 * @param password
	 * @return ResponseEntity Object along with status code and message
	 * @throws UserExistsErrorException
	 * @throws CredentialErrorException
	 * @author Pallavi this method is used for login
	 * @throws UserNameCredentialException 
	 */
	@PostMapping(value = "/login")
	public ResponseEntity<CustomerDto> checkLoginByUserId(@RequestBody LoginRequestDto loginRequestDto) throws UserExistsErrorException {

		logger.info("Inside CustomerController of customerLogin method ");
		return new ResponseEntity<>(customerService.customerLogin(loginRequestDto), HttpStatus.ACCEPTED);
	}
	
	

	/**
	 * 
	 *
	 * @request Body fundTransferRequestDto
	 * 
	 * @return ResponseEntity Object along with status code and message
	 * @throws UserExistsErrorException
	 * 
	 * @author Kusuma thismethod is used for fundTransfer
	 * @throws FundsException 
	 * @throws UserAccountNotFoundException 
	 * @throws BeneficiaryAccountNotFoundException 
	 */
	@PostMapping(value = "/fundtransfer")
	public ResponseEntity<String> fundTransfer(@RequestBody FundTransferRequestDto fundTransferRequestDto)
			throws UserExistsErrorException, UserAccountNotFoundException, FundsException, BeneficiaryAccountNotFoundException {

		logger.info("Inside CustomerController of fundTransfer method ");
		return new ResponseEntity<>(customerService.fundTransfer(fundTransferRequestDto), HttpStatus.OK);
	}

	/**
	 * 
	 *
	 * @param accountNumber
	 * @param beneficiaryAccountNumber
	 * 
	 * @return ResponseEntity Object along with status code and message
	 * @throws UserExistsErrorException
	 * 
	 * @author Suprita this method is used for adding beneficiers
	 * @throws BeneficiaryAlreadyExists 
	 * @throws BeneficiaryAccountNotFoundException 
	 * @throws UserAccountNotFoundException 
	 */
	@PostMapping("/beneficiary")
	public ResponseEntity<String> addBeneficiary(@RequestParam("accountNumber") String accountNumber,
			@RequestParam("beneficiaryAccountNumber") String beneficiaryAccountNumber) throws UserExistsErrorException, UserAccountNotFoundException, BeneficiaryAccountNotFoundException, BeneficiaryAlreadyExists {
		logger.info("Inside CustomerController of addBeneficiary method ");
		return new ResponseEntity<>(customerService.addBeneficiary(accountNumber, beneficiaryAccountNumber),
				HttpStatus.OK);
	}

	/**
	 * 
	 * @param accountNumber
	 * @return
	 * @throws UserExistsErrorException
	 * @throws ListEmptyException
	 */
	@GetMapping("/{accountNumber}")
	public ResponseEntity<List<Object>> bankStatement(@PathVariable String accountNumber) throws UserExistsErrorException, ListEmptyException {
		return new ResponseEntity<>(customerService.bankStatement(accountNumber), HttpStatus.OK);

	}
	
	/**
	 * 
	 * @return
	 * @throws UserNotLoginException 
	 * @throws InvalidLoginException
	 */
	@GetMapping("/beneficiaryList")
	public ResponseEntity<List<String>> beneficiaryList() throws UserNotLoginException {
		return new ResponseEntity<>(customerService.beneficiaryList(), HttpStatus.OK);
	}
}
