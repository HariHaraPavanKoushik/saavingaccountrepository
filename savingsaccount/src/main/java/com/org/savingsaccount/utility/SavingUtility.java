package com.org.savingsaccount.utility;

public class SavingUtility {
	
	public SavingUtility() {
		
	}
	
	public static final String USER_ACCOUNT_NUMBER_NOT_FOUND = "User Account number not found";
	public static final int USER_ACCOUNT_NUMBER_NOT_FOUND_STATUS = 620;

	public static final String BENEFICIARY_ACCOUNT_NUMBER_NOT_FOUND = "Beneficary Account number not found";
	public static final int BENEFICIARY_ACCOUNT_NUMBER_NOT_FOUND_STATUS_CODE = 625;
	
	public static final String BENEFICIARY_ACCOUNT_NUMBER_EXISTS = "Beneficary Account number already exist";
	public static final int BENEFICIARY_ACCOUNT_NUMBER_EXISTS_STATUS_CODE = 630;
	
	public static final String BENEFICIARY_ADDED_SUCCESS = "Beneficary Account added successfully";
	public static final int BENEFICIARY_ADDED_SUCCESS_STATUS_CODE = 635;
	
	public static final String LIST_EMPTY = "No History Found for this Record";
	public static final int LIST_EMPTY_STATUS_CODE = 640;
}
