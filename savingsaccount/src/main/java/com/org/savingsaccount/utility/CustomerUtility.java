package com.org.savingsaccount.utility;

public class CustomerUtility {
	
	public CustomerUtility() {
		
	}
	
	public static final String CREDENTIAL_ERROR = "Password is Incorrect!";
	public static final int PASSWORD_INCOORECT_STATUS = 605;
	
	public static final String USERNAME_CREDENTIAL_ERROR = "Email is Incorrect!";
	public static final int USERNAME_CREDENTIAL_ERROR_STATUS = 610;

	public static final String USER_ERROR = "EmailId is not exsist Please Register";
	public static final int USER_ERROR_STATUS = 600;
	
	public static final String INSUFFICIENT_BALANCE = "Insufficient balance";
	public static final int INSUFFICIENT_BALANCE_STAUS_CODE = 615;
	
	public static final String USERNOTLOGIN_ERROR = "User is not logged in";
	public static final int USERNOTLOGIN_ERROR_STATUS = 645;

}
