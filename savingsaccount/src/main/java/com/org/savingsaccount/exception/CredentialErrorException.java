package com.org.savingsaccount.exception;

public class CredentialErrorException extends Exception {
	private static final long serialVersionUID = 1L;

	public CredentialErrorException(String message) {
		super(message);
	}

}
