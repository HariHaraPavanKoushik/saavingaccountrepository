package com.org.savingsaccount.exception;

public class ListEmptyException extends Exception{

	private static final long serialVersionUID = 1L;

	public ListEmptyException(String message) {
		super(message);
	}
}
