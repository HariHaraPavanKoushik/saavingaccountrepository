package com.org.savingsaccount.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.org.savingsaccount.utility.CustomerUtility;
import com.org.savingsaccount.utility.SavingUtility;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(UserExistsErrorException.class)
	public ResponseEntity<ErrorResponse> userExistsErrorException(UserExistsErrorException ex) {

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(CustomerUtility.USER_ERROR_STATUS);

		
		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);

	}

	@ExceptionHandler(CredentialErrorException.class)
	public ResponseEntity<ErrorResponse> credentialErrorException(CredentialErrorException ex) {

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(CustomerUtility.PASSWORD_INCOORECT_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);

	}
	
	@ExceptionHandler(UserNameCredentialException.class)
	public ResponseEntity<ErrorResponse> userNameCredentialException(CredentialErrorException ex) {

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(CustomerUtility.USERNAME_CREDENTIAL_ERROR_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);

	}
	
	@ExceptionHandler(UserAccountNotFoundException.class)
	public ResponseEntity<ErrorResponse> userAccountNotFoundException(UserAccountNotFoundException ex) {

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(SavingUtility.USER_ACCOUNT_NUMBER_NOT_FOUND_STATUS);

		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);

	}
	
	@ExceptionHandler(FundsException.class)
	public ResponseEntity<ErrorResponse> fundsException(FundsException ex) {

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(CustomerUtility.INSUFFICIENT_BALANCE_STAUS_CODE);

		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);

	}
	
	@ExceptionHandler(BeneficiaryAlreadyExists.class)
	public ResponseEntity<ErrorResponse> beneficiaryAlreadyExists(BeneficiaryAlreadyExists ex) {

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(SavingUtility.BENEFICIARY_ACCOUNT_NUMBER_EXISTS_STATUS_CODE);

		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);

	}
	
	@ExceptionHandler(BeneficiaryAccountNotFoundException.class)
	public ResponseEntity<ErrorResponse> beneficiaryAccountNotFoundException(BeneficiaryAccountNotFoundException ex) {

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(SavingUtility.BENEFICIARY_ACCOUNT_NUMBER_NOT_FOUND_STATUS_CODE);

		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);

	}

	@ExceptionHandler(BeneficiaryAddedSuccess.class)
	public ResponseEntity<ErrorResponse> beneficiaryAddedSuccess(BeneficiaryAddedSuccess ex) {

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(SavingUtility.BENEFICIARY_ADDED_SUCCESS_STATUS_CODE);

		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);

	}
	
	@ExceptionHandler(ListEmptyException.class)
	public ResponseEntity<ErrorResponse> listEmptyException(ListEmptyException ex) {

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(SavingUtility.LIST_EMPTY_STATUS_CODE);

		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);

	}
}
