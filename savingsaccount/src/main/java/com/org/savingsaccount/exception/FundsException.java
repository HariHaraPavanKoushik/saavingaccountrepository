package com.org.savingsaccount.exception;

public class FundsException extends Exception {

	private static final long serialVersionUID = 1L;

	public FundsException(String message) {
		super(message);
	}

}
