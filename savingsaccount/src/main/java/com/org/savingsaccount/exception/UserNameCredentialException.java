package com.org.savingsaccount.exception;

public class UserNameCredentialException extends Exception{

	private static final long serialVersionUID = 1L;

	public UserNameCredentialException(String message) {
		super(message);
	}
}
