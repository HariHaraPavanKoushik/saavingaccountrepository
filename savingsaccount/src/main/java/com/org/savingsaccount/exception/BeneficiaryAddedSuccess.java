package com.org.savingsaccount.exception;

public class BeneficiaryAddedSuccess extends Exception{

	private static final long serialVersionUID = 1L;

	public BeneficiaryAddedSuccess(String message) {
		super(message);
	}
}
