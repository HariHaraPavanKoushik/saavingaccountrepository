package com.org.savingsaccount.exception;

public class BeneficiaryAlreadyExists extends Exception{

	private static final long serialVersionUID = 1L;

	public BeneficiaryAlreadyExists(String message) {
		super(message);
	}

}
