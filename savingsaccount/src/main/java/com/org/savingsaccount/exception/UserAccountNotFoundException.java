package com.org.savingsaccount.exception;

public class UserAccountNotFoundException extends Exception{

	private static final long serialVersionUID = 1L;

	public UserAccountNotFoundException(String message) {
		super(message);
	}

}
