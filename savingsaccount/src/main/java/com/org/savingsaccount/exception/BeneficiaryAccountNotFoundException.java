package com.org.savingsaccount.exception;

public class BeneficiaryAccountNotFoundException extends Exception{

	private static final long serialVersionUID = 1L;

	public BeneficiaryAccountNotFoundException(String message) {
		super(message);
	}
}
