package com.org.savingsaccount.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.org.savingsaccount.entity.SelfAccountTransaction;
import com.org.savingsaccount.exception.UserExistsErrorException;

@Repository
public interface SelfAccountTransactionRepository extends JpaRepository<SelfAccountTransaction, Long> {
	Optional<List<SelfAccountTransaction>> findByAccountNumber(String accountNumber) throws UserExistsErrorException;

}
