package com.org.savingsaccount.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.org.savingsaccount.entity.Beneficiary;

@Repository
public interface BeneficiaryRepository extends JpaRepository<Beneficiary, Long> {
	public Optional<Beneficiary> findByBeneficiaryAccountNumberAndFromAccountNumber(String beneficiaryAccountNumber,
			String fromAccountNumber);

	Optional<Beneficiary> findByBeneficiaryAccountNumber(String beneficiaryAccountNumber);

	public List<Beneficiary> findByFromAccountNumber(String accountNumber);

}
