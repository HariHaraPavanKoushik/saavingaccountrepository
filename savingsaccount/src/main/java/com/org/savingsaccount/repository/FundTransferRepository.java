package com.org.savingsaccount.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.org.savingsaccount.entity.FundTransfer;
import com.org.savingsaccount.exception.UserExistsErrorException;

@Repository
public interface FundTransferRepository extends JpaRepository<FundTransfer, Long> {

	Optional<List<FundTransfer>> findByFromAccountNumberOrToAccountNumber(String fromAccountNumber,
			String toAccountNumber) throws UserExistsErrorException;

}
