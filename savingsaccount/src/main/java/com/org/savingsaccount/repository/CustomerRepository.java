package com.org.savingsaccount.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.org.savingsaccount.entity.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

	Optional<Customer> findByEmailId(String emailId);

	Optional<Customer> findByAccountNumber(String accountNumber);

	Optional<Customer> findByEmailIdAndPassword(String email, String password);
	
}
