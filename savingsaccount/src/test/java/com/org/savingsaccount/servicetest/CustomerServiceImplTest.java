package com.org.savingsaccount.servicetest;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import com.org.savingsaccount.dto.CustomerDto;
import com.org.savingsaccount.dto.FundTransferRequestDto;
import com.org.savingsaccount.dto.LoginRequestDto;
import com.org.savingsaccount.entity.Beneficiary;
import com.org.savingsaccount.entity.Customer;
import com.org.savingsaccount.entity.FundTransfer;
import com.org.savingsaccount.entity.SelfAccountTransaction;
import com.org.savingsaccount.exception.BeneficiaryAccountNotFoundException;
import com.org.savingsaccount.exception.BeneficiaryAlreadyExists;
import com.org.savingsaccount.exception.FundsException;
import com.org.savingsaccount.exception.ListEmptyException;
import com.org.savingsaccount.exception.UserAccountNotFoundException;
import com.org.savingsaccount.exception.UserExistsErrorException;
import com.org.savingsaccount.exception.UserNotLoginException;
import com.org.savingsaccount.repository.BeneficiaryRepository;
import com.org.savingsaccount.repository.CustomerRepository;
import com.org.savingsaccount.repository.FundTransferRepository;
import com.org.savingsaccount.repository.SelfAccountTransactionRepository;
import com.org.savingsaccount.service.CustomerServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceImplTest {

	@Mock
	CustomerRepository customerRepository;

	@Mock
	BeneficiaryRepository beneficiaryRepository;

	@Mock
	FundTransferRepository fundTransferRepository;

	@Autowired
	SelfAccountTransactionRepository selfAccountTransactionRepository;

	@InjectMocks
	CustomerServiceImpl customerServiceImpl;
	FundTransferRequestDto fundTransferRequestDto;

	Customer customer;
	Customer customer2;
	Beneficiary beneficiary;
	CustomerDto customerDto;
	LoginRequestDto loginRequestDto;
	String emailId, password, accountNumber, beneficiaryAccountNumber;
	FundTransfer fundTransfer;
	List<FundTransfer> listFundTransfer;
	List<SelfAccountTransaction> listSelfAccountTransaction;
	List<Beneficiary> beneficiaryList;
	SelfAccountTransaction selfAccountTransaction;

	@Before
	public void setup() {
		customer = new Customer();

		customer2 = new Customer();

		customer.setAccountNumber("1234567");
		customer.setAccountType("savings");
		customer.setBalance(1000000);
		customer.setBankName("SBI");
		customer.setCustomerId(1l);
		customer.setCustomerName("pallavi");
		customer.setEmailId("pallavi@hcl.com");
		customer.setIfscCode("SBI001N");
		customer.setPassword("123");
		customer.setPhoneNumber("1234567");

		customer2.setAccountNumber("0987654");
		customer2.setAccountType("savings");
		customer2.setBalance(1000000);
		customer2.setBankName("SBI");
		customer2.setCustomerId(2l);
		customer2.setCustomerName("pallavi");
		customer2.setEmailId("suprita@hcl.com");
		customer2.setIfscCode("ING0012");
		customer2.setPassword("456");
		customer2.setPhoneNumber("09876543");

		customerDto = new CustomerDto();
		emailId = "pallavi@hcl.com";
		password = "123";

		beneficiary = new Beneficiary();
		beneficiary.setBankName("ING");
		beneficiary.setBeneficiaryAccountNumber("0987654");
		beneficiary.setBeneficiaryId(2l);
		beneficiary.setFromAccountNumber("1234567");
		beneficiary.setIfscCode("ING0012");

		fundTransferRequestDto = new FundTransferRequestDto();
		fundTransferRequestDto.setAmount(100);
		fundTransferRequestDto.setFromAccountNumber("1234567");
		fundTransferRequestDto.setToAccountNumber("0987654");

		fundTransfer = new FundTransfer();
		fundTransfer.setAmount(1000000);
		fundTransfer.setDate(LocalDateTime.now());
		fundTransfer.setFromAccountNumber("1234567");
		fundTransfer.setFundTransferId(1l);
		fundTransfer.setToAccountNumber("0987654");

		accountNumber = "1234567";
		beneficiaryAccountNumber = "10987654";

		listFundTransfer = new ArrayList<>();

		listSelfAccountTransaction = new ArrayList<>();
		selfAccountTransaction.setAccountNumber("12345");
		selfAccountTransaction.setAmount(100);
		selfAccountTransaction.setDate(LocalDateTime.now());
		selfAccountTransaction.setSelfAccountTransactionId(1l);
		selfAccountTransaction.setTransactionType("withdrawl");

		listSelfAccountTransaction.add(selfAccountTransaction);

		loginRequestDto = new LoginRequestDto();
		loginRequestDto.setEmail("pallavi@hcl.com");
		loginRequestDto.setPassword("123");

		beneficiaryList = new ArrayList<>();
	}

	@Test
	public void customerLogin() throws UserExistsErrorException  {

		Mockito.when(customerRepository.findByEmailIdAndPassword(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(Optional.of(customer));
		CustomerDto response = customerServiceImpl.customerLogin(loginRequestDto);
		Assert.assertNotNull(response);
		Assert.assertEquals(response.getAccountNumber(), customer.getAccountNumber());
	}

	@Test(expected = UserExistsErrorException.class)
	public void customerLoginEmailIdError() throws UserExistsErrorException {

		Mockito.when(customerRepository.findByEmailIdAndPassword(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(Optional.empty());

		customerServiceImpl.customerLogin(loginRequestDto);

	}

	@Test
	public void fundTransfer() throws BeneficiaryAccountNotFoundException, UserAccountNotFoundException, FundsException  {
		Mockito.when(beneficiaryRepository.findByBeneficiaryAccountNumberAndFromAccountNumber(Mockito.anyString(),
				Mockito.anyString())).thenReturn(Optional.of(beneficiary));
		Mockito.when(customerRepository.findByAccountNumber(Mockito.anyString())).thenReturn(Optional.of(customer));
		Mockito.when(customerRepository.save(Mockito.any(Customer.class))).thenReturn(customer);
		Mockito.when(customerRepository.save(Mockito.any(Customer.class))).thenReturn(customer2);
		Mockito.when(fundTransferRepository.save(Mockito.any(FundTransfer.class))).thenReturn(fundTransfer);
		String response = customerServiceImpl.fundTransfer(fundTransferRequestDto);
		Assert.assertNotNull(response);
		Assert.assertEquals(response, "Transferred");

	}

	@Test(expected = BeneficiaryAccountNotFoundException.class)
	public void fundTransferUserNotFound() throws BeneficiaryAccountNotFoundException, UserAccountNotFoundException, FundsException {

		Mockito.when(beneficiaryRepository.findByBeneficiaryAccountNumberAndFromAccountNumber(Mockito.anyString(),
				Mockito.anyString())).thenReturn(Optional.empty());

		customerServiceImpl.fundTransfer(fundTransferRequestDto);
	}

	@Test(expected = UserAccountNotFoundException.class)
	public void fundTransferFromUserNotFound() throws BeneficiaryAccountNotFoundException, UserAccountNotFoundException, FundsException  {

		Mockito.when(beneficiaryRepository.findByBeneficiaryAccountNumberAndFromAccountNumber(Mockito.anyString(),
				Mockito.anyString())).thenReturn(Optional.of(beneficiary));
		Mockito.when(customerRepository.findByAccountNumber(Mockito.anyString())).thenReturn(Optional.empty());

		customerServiceImpl.fundTransfer(fundTransferRequestDto);
	}

	@Test(expected = BeneficiaryAccountNotFoundException.class)
	public void fundTransferToUserNotFound() throws BeneficiaryAccountNotFoundException, UserAccountNotFoundException, FundsException{

		Mockito.when(beneficiaryRepository.findByBeneficiaryAccountNumberAndFromAccountNumber(Mockito.anyString(),
				Mockito.anyString())).thenReturn(Optional.of(beneficiary));
		Mockito.when(customerRepository.findByAccountNumber(customer.getAccountNumber()))
				.thenReturn(Optional.of(customer));
		Mockito.when(customerRepository.findByAccountNumber(customer2.getAccountNumber())).thenReturn(Optional.empty());

		customerServiceImpl.fundTransfer(fundTransferRequestDto);
	}

	@Test(expected = FundsException.class)
	public void fundTransferInsufficientBalance() throws BeneficiaryAccountNotFoundException, UserAccountNotFoundException, FundsException  {
		customer.setBalance(90);
		Mockito.when(beneficiaryRepository.findByBeneficiaryAccountNumberAndFromAccountNumber(Mockito.anyString(),
				Mockito.anyString())).thenReturn(Optional.of(beneficiary));
		Mockito.when(customerRepository.findByAccountNumber(Mockito.anyString())).thenReturn(Optional.of(customer));
		Mockito.when(customerRepository.save(Mockito.any(Customer.class))).thenReturn(customer);
		customerServiceImpl.fundTransfer(fundTransferRequestDto);
	}

	@Test(expected = FundsException.class)
	public void fundTransferMinBalance() throws BeneficiaryAccountNotFoundException, UserAccountNotFoundException, FundsException  {
		customer.setBalance(1000);
		Mockito.when(beneficiaryRepository.findByBeneficiaryAccountNumberAndFromAccountNumber(Mockito.anyString(),
				Mockito.anyString())).thenReturn(Optional.of(beneficiary));
		Mockito.when(customerRepository.findByAccountNumber(Mockito.anyString())).thenReturn(Optional.of(customer));
		Mockito.when(customerRepository.save(Mockito.any(Customer.class))).thenReturn(customer);
		customerServiceImpl.fundTransfer(fundTransferRequestDto);
	}

	@Test
	public void addBeneficiary() throws BeneficiaryAlreadyExists, UserAccountNotFoundException, BeneficiaryAccountNotFoundException {
		Mockito.when(customerRepository.findByAccountNumber(accountNumber)).thenReturn(Optional.of(customer));

		Mockito.when(customerRepository.findByAccountNumber(beneficiaryAccountNumber))
				.thenReturn(Optional.of(customer2));

		String response = customerServiceImpl.addBeneficiary(accountNumber, beneficiaryAccountNumber);
		Assert.assertNotNull(response);
		Assert.assertEquals(response, "Beneficary Account added successfully");

	}

	@Test(expected = UserAccountNotFoundException.class)
	public void addBeneficiaryFromAccountError() throws BeneficiaryAlreadyExists, UserAccountNotFoundException, BeneficiaryAccountNotFoundException  {
		Mockito.when(customerRepository.findByAccountNumber(Mockito.anyString())).thenReturn(Optional.empty());

		customerServiceImpl.addBeneficiary(accountNumber, beneficiaryAccountNumber);

	}

	@Test(expected = BeneficiaryAccountNotFoundException.class)
	public void addBeneficiaryToAcct() throws BeneficiaryAlreadyExists, UserAccountNotFoundException, BeneficiaryAccountNotFoundException  {
		Mockito.when(customerRepository.findByAccountNumber(customer.getAccountNumber()))
				.thenReturn(Optional.of(customer));

		Mockito.when(customerRepository.findByAccountNumber(customer2.getAccountNumber())).thenReturn(Optional.empty());
		customerServiceImpl.addBeneficiary(accountNumber, beneficiaryAccountNumber);
	}

	@Test(expected = BeneficiaryAlreadyExists.class)
	public void addBeneficiaryBeneficiaryError() throws BeneficiaryAlreadyExists, UserAccountNotFoundException, BeneficiaryAccountNotFoundException  {

		Mockito.when(customerRepository.findByAccountNumber(accountNumber)).thenReturn(Optional.of(customer));

		Mockito.when(customerRepository.findByAccountNumber(beneficiaryAccountNumber))
				.thenReturn(Optional.of(customer2));
		Mockito.when(beneficiaryRepository.findByBeneficiaryAccountNumber(beneficiaryAccountNumber))
				.thenReturn(Optional.of(beneficiary));

		customerServiceImpl.addBeneficiary(accountNumber, beneficiaryAccountNumber);

	}

	@Test
	public void bankStatement() throws UserExistsErrorException, ListEmptyException {
		Mockito.when(fundTransferRepository.findByFromAccountNumberOrToAccountNumber(Mockito.anyString(),
				Mockito.anyString())).thenReturn(Optional.of(listFundTransfer));
		Mockito.when(selfAccountTransactionRepository.findByAccountNumber(Mockito.anyString()))
				.thenReturn(Optional.of(listSelfAccountTransaction));
		List<Object> response = customerServiceImpl.bankStatement(accountNumber);
		List<Object> obj = new ArrayList<>();
		Assert.assertEquals(response.size(), obj.size());
	}

	
	@Test
	public void beneficiaryList() throws UserNotLoginException {
		Mockito.when(beneficiaryRepository.findByFromAccountNumber(Mockito.anyString())).thenReturn(beneficiaryList);

		List<String> response = customerServiceImpl.beneficiaryList();
		Assert.assertNotNull(response);
		Assert.assertEquals(response, beneficiaryList.get(0).getBeneficiaryAccountNumber());

	}

}
