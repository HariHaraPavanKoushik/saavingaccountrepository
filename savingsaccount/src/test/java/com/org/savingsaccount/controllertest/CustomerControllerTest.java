package com.org.savingsaccount.controllertest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.org.savingsaccount.controller.CustomerController;
import com.org.savingsaccount.dto.CustomerDto;
import com.org.savingsaccount.dto.FundTransferRequestDto;
import com.org.savingsaccount.dto.LoginRequestDto;
import com.org.savingsaccount.exception.BeneficiaryAccountNotFoundException;
import com.org.savingsaccount.exception.BeneficiaryAlreadyExists;
import com.org.savingsaccount.exception.FundsException;
import com.org.savingsaccount.exception.ListEmptyException;
import com.org.savingsaccount.exception.UserAccountNotFoundException;
import com.org.savingsaccount.exception.UserExistsErrorException;
import com.org.savingsaccount.exception.UserNotLoginException;
import com.org.savingsaccount.service.CustomerService;

@RunWith(MockitoJUnitRunner.class)

public class CustomerControllerTest {

	@Mock
	CustomerService customerService;

	@InjectMocks
	CustomerController customerController;

	CustomerDto customerDto;

	FundTransferRequestDto fundTransferRequestDto;

	String accountNumber, beneficiaryAccountNumber;

	LoginRequestDto loginRequestDto;

	List<Object> obj;

	@Before
	public void setup() {
		customerDto = new CustomerDto();
		customerDto.setAccountNumber("1234567");
		customerDto.setBalance(12334);
		customerDto.setBankName("SBI");
		customerDto.setIfscCode("SBI001");
		customerDto.setCustomerName("pallavi");

		fundTransferRequestDto = new FundTransferRequestDto();
		fundTransferRequestDto.setAmount(100);
		fundTransferRequestDto.setFromAccountNumber("1234567");
		fundTransferRequestDto.setToAccountNumber("0987654");

		loginRequestDto = new LoginRequestDto();
		loginRequestDto.setEmail("pallavi@hcl.com");
		loginRequestDto.setPassword("123");

		accountNumber = "1234567";
		beneficiaryAccountNumber = "0987654";

		obj = new ArrayList<Object>();
	}

	@SuppressWarnings("deprecation")
	@Test
	public void customerLoginTest() throws UserExistsErrorException {
		Mockito.when(customerService.customerLogin(Mockito.anyObject())).thenReturn(customerDto);

		ResponseEntity<CustomerDto> response = customerController.checkLoginByUserId(loginRequestDto);

		Assert.assertNotNull(response);

		Assert.assertEquals(customerDto, response.getBody());

	}

	@Test
	public void fundTransfer() throws BeneficiaryAccountNotFoundException, UserAccountNotFoundException, FundsException, UserExistsErrorException  {
		String data = "Transfered successfully";

		Mockito.when(customerService.fundTransfer(Mockito.any(FundTransferRequestDto.class))).thenReturn(data);

		ResponseEntity<String> response = customerController.fundTransfer(fundTransferRequestDto);

		Assert.assertNotNull(response);

		Assert.assertEquals("Transfered successfully", response.getBody());
	}

	@Test
	public void addBeneficiary() throws UserExistsErrorException, UserAccountNotFoundException, BeneficiaryAccountNotFoundException, BeneficiaryAlreadyExists  {
		String data = "Beneficary Account added successfully";
		Mockito.when(customerService.addBeneficiary(Mockito.anyString(), Mockito.anyString())).thenReturn(data);
		ResponseEntity<String> response = customerController.addBeneficiary(accountNumber, beneficiaryAccountNumber);
		Assert.assertNotNull(response);

		Assert.assertEquals("Beneficary Account added successfully", response.getBody());
	}

	@Test
	public void bankStatement() throws ListEmptyException, UserExistsErrorException {

		Mockito.when(customerService.bankStatement(Mockito.anyString())).thenReturn(obj);
		ResponseEntity<List<Object>> response = customerController.bankStatement(accountNumber);
		Assert.assertNotNull(response);

		Assert.assertEquals(obj, response.getBody());
	}

	@Test
	public void beneficiaryList() throws UserNotLoginException  {
		List<String> beneficiaryNameList = new ArrayList<>();

		Mockito.when(customerService.beneficiaryList()).thenReturn(beneficiaryNameList);
		ResponseEntity<List<String>> response = customerController.beneficiaryList();
		Assert.assertNotNull(response);

		Assert.assertEquals(beneficiaryNameList, response.getBody());
	}

}
